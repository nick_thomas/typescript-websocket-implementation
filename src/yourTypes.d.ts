import { Server, Socket } from "net";

declare global {
  interface socketClient extends Socket {
    closeFlag: boolean;
    fragment: Buffer[];
    opcode: number;
    sessionId: string;
  }

  interface socketFrame {
    fin: boolean;
    rsv1: string;
    rsv2: string;
    rsv3: string;
    opcode: number;
    mask: boolean;
    len: number;
    maskKey: Buffer;
    payload: Buffer;
  }

  interface websocket {
    broadcast: (data: Buffer | string) => void;
    clientList: socketClient[];
    send: (socket: socketClient, data: Buffer | string) => void;
    server: (config: websocketServer) => Server;
  }

  interface websocketServer {
    address: string;
    callback: (port: number) => void;
    cert: {
      cert: string;
      key: string;
    };
    port: number;
  }
}
