import { createHash, Hash } from "crypto";
import { AddressInfo, createServer as netServer, Server } from "net";
import { createServer as tlsServer } from "tls";

const websocket: websocket = {
  broadcast: function (data: Buffer | string): void {},
  clientList: socketClient[],
  send: function (socket: socketClient,data: Buffer | string): void {
    // data is fragmented above 1 million bytes and send unmasked
    if (socket.closeFlag === true){
      return;
    }
    let len:number = 0;
    let dataPackage:Buffer = null;
    // first two frames are required header,simplified headers because its all unmaksed
    // * payload size smaller than 126 bytes
    // - 0 allocated bytes for extended lenght value
    // -length value in byte index 1 as payload length
    // * payload size 126 - 65535 bytes
    // - 2 bytes allocated for extended length value (indexes 2 and 3)
    // - length value in byte index 1 is 126
    // * payload size larger than 65535 bytes
    // - 8 bytes allocated for extended length value (indexes 2 - 9)
    // -length value in byte index 1 is 127
    let stringData: string = null;
    let fragment: Buffer = null;
    let frame : Buffer = null;
    const socketData: socketData = payload as socketData;
    const isBuffer: boolean = (socketData.service === undefined);
    // fragmentation is disabled by assigning a value of 0
    const fragmentSize:number = 1e6;
    const op: 1| 2 | 8 | 9 = (opcode === undefined) ? (isBuffer === true) ? 2 : 1 : opcode;
    const writeFrame = function terminal_server_transmission_transmitWs_send_writeFrame(finish: boolean, firstFrame: boolean): void
    {
      const size:number = fragment.length;
      // frame 0 is:
      // *127 bits for fin, 0 for unfinished plus opcode
      // * opcode 0 - continuation of fragments
      // * opcode 1 - text (totaly payload must be UTF8 and probably not contain hidden control characters)
      // * opcode 2 - supposed to be binary, really anything that isnt 100& UTF8 text
      // ** for fragmented data only first data frame gets a data opcode, others receive 0 (continuity)
      frame[0] = (finish === true) ? (firstFrame === true) ? 128 + op : 128: (firstFrame === true) ? op : 0;
      // frame 1 is length flag
      frame[1] = (size<126) ? size : (size <65536 ? 126 : 127);
      if (size > 125){
        if (size < 65536) {
          frame.writeUInt16BE(size,2);
        } else {
          frame.writeUIntBE(size, 4, 6);
        }
      }
      socket.write(Buffer.concat([frame,fragment]));
    },
    const fragmentation = function terminal_server_transmission_transmitWs_send_fragmentation(first:boolean): void {
      if (len > fragmentSize && fragmentSize > 0){
        fragment = (isBuffer === true) ? dataPackage.slice(0,fragmentSize) : Buffer.from(stringData.slice(0,fragmentSize),"utf8");
        // fragmentation
        if (first === true) {
          // first frame of fragment
          writeFrame(false,true);
        } else if (len > fragmentSize){
          // continuation of fragment
          writeFrame(false,false);
        }
        if (isBuffer === true){
          dataPackage = dataPackage.slice(fragmentSize);
          len = dataPackage.length;
        } else {
          stringData = stringData.slice(fragmentSize);
          len = Buffer.byteLength(stringData,"utf8")
        }
        terminal_server_transmission_transmitWs_send_fragmentation(false);
      } else {
        // finished not fragmented if first === true
        fragment = (isBuffer === true) ? dataPackage : Buffer.from(stringData, "utf8");
        writeFrame(true,first);
      }
    };
    if (isBuffer === false){
      stringData = JSON.stringify(payload);
    }
    dataPackage = (isBuffer === true) ? payload as Buffer : Buffer.from(stringData, "utf8");
    len = dataPackage.length;
    frame = (len <126) ? Buffer.alloc(2) : (len < 65536) ? Buffer.alloc(4) : Buffer.alloc(10);
    fragmentation(true);
  },
  server: function(config: websocketServer): Server {
    // create the server
    const wsServer: Server = (config:cert === null) ? netServer() : tlsServer({
      cert: config.cert.cert,
      key: config.cert.key,
      requestCert: true
    }),
    // server created
   const handshake = function (socket: socketClient,data: string, callback: (key: string) => void): void{
      const headers: string[] = data.split("\r\n"),
    },
    const datahandler = function(socket: socketClient): void {
      const processor = function(data: Buffer):void {
        // decode the frame header
        const toBin = function terminal_server_transmission_websocket_listener_processor_convertBin(input:number): string {
          return (input >>> 0).toString(2);
        },
        const toDec = function terminal_server_transmission_websocket_listener_processor_convertDec(input:string):number {
          return parseInt(input,2);
        }
        const frame: socketFrame = (function terminal_server_transmission_websocket_listener_processor_frame(): socketFrame {
          const bits0:string  = toBin(data[0]);
          const bits1:string = toBin(data[1]);
          const frameItem: socketFrame = {
            fin: (bits0.charAt(0) === "1"),
            rsv1: bits0.charAt(1),
            rsv2: bits0.charAt(2),
            rsv3: bits0.charAt(3),
            opcode: toDec(bits0.slice(4)),
            mask: (bits1.charAt(0) === "1"),
            len: toDec(bits1.slice(1)),
            extended: 0,
            maskKey: null,
            payload: null
          };
          const startByte: number = (function terminal_server_transmission_websocket_listener_processor_frame_startByte(): number{
            const keyOffset:number = (frameItem.mask === true) ? 4 : 0;
            if (frameItem.len < 126){
              frameItem.extended = frameItem.len;
              return 2 + keyOffset;
            }
            if (frameItem.len < 127){
              frameItem.extended = data.slice(2,4).readUInt16BE(0);
              return 4 + keyOffset;
            }
            frameItem.extended = data.slice(4,10).readUIntBE(0, 6);
            return 10 + keyOffset;
          }());
          if (frameItem.mask === true){
            frameItem.maskKey = data.slice(startByte -4, startByte);
          }
          frameItem.payload = data.slice(startByte);
          return frameItem;
        }());
        // decoding completed

        // unmask payload
        if (frame.mask === true){
          /* RFC 6455, 5.3 Client-to-Server Masking
            j = i MOD 4
            transformed-octet-i = original-octet-i XOR masking-key-octet-j
          */
         frame.payload.forEach(function terminal_commands_websocket_dataHandler_unmask(value: number,index: number): void{
           frame.payload[index] = value ^ frame.maskKey[index % 4];
         });
        }
        // unmask payload complete

        socket.fragment.push(frame.payload);

        // store payload or write response
        if (frame.fin === true){
          // complete data frame
          const opcode:number = (frame.opcode === 0)? socket.opcode : frame.opcode;
          const control = function terminal_commands_websocket_dataHandler_control(): void {
            if (opcode === 8){
              // remove closed socket from client list
              let a: number = websocket.clientList.length;
              do{
                a = a-1;
                if (websocket.clientList[a].sessionId === socket.sessionId){
                  websocket.clientList.splice(a,1);
                  break;
                }
              } while (a > 0);
              socket.closeFlag = true;
            } else if (opcode === 9) {
              // respond to "ping" as "pong"
              data[0] = websocket.convert.toDec(`1${frame.rsv1 + frame.rsv2 + frame.rsv3}1010`);
            }
            data[1] = websocket.convert.toDec(`0${websocket.convert.toBin(frame.payload.length)}`);
            socket.write(Buffer.concat([data.slice(0,2), frame.payload]));
            if (opcode === 8){
              // end the closed socket
              socket.end();
            }
          };

          // write frame header + payload
          if (opcode === 1 || opcode === 2){
            //text or binary
          const result: string = Buffer.concat(socket.fragment).slice(0,frame.extended).toString();

          // handle the result here !!!!

          // reset socket
          socket.fragment = [];
          socket.opcode = 0;
          } else {
            control();
          }
        } else {
          // fragment, must be type text (1) or binary (2)
          if (frame.opcode > 0){
            socket.opcode = frame.opcode;
          }
        }
      };
      socket.on("data",processor);
    };

    wsServer.listen({
      host: config.address,
      port: config.port
    }, function (): void {
      const addressInfo: AddressInfo = wsServer.address() as AddressInfo;
      config.callback(addressInfo.port);
    });
    // server is listening

    // assign an event handler to the connection event
    wsServer.on("connection", function (socket: socketClient): void {
      const handshakeHandler = function (data: Buffer): void {
        const handshakeCallback = function (key: string): void {

          // modify the socket for use in application
          socket.closeFlag = false; // closeFlag - wehther the socket is (or about to be) closed, do not write
          socket.fragment = [];     // storehouse of data received for a fragment data package
          socket.opcode = 0;        // stores opcode of fragmented data page (1 or 2), because additional fragment frames have code 0 (continuity)
          socket.sessionId = key;   // a unique identifier on which to identify and differental this socket from other client sockets
          socket.setKeepAlive(true, 0); // standard method to retain socket against time from inactivity until a close frame comes in
          socket.clientList.push(socket); // push this socket into the list of socket clients

          // change the listener to process data
          socket.removeListener("data", terminal_commands_websocket_connection_handshakeHandler);
          dataHandler(socket);
        };
        handshake(socket,data.toString(),handshakeCallback);
      };
        socket.on("data",handshakeHandler);
        socket.on("error", function (errorItem: error){
          if (socket.closeFlag === false) {
            console.error(errorItem.toString());
          }
        });
    });
    return wsServer;
  }
};

export default websocket
